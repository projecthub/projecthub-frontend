import { useState } from 'react';
import './App.css';
import { Logic } from './logic';
import { Navigation } from './navigation/Navigation';
// delete later

import constants from "./logic/constants"
const App = (props: { logic: Logic }) => {
  const [route, setRoute] = useState<string[]>(props.logic.router.route)
  props.logic.router.onRouteChange = (newRoute) => {
    setRoute(newRoute)
  }
  const setChildRouteOnRouter = (newRoute: string[]) => {
    props.logic.router.setRoute(newRoute)
  }
  return (
    <Navigation layout={constants.layout} setRoute={setChildRouteOnRouter} route={route} logic={props.logic} />
  );
}

export default App;
