import React from 'react';
import { GalleryDTO } from '../../types/Project';
import ImageGallery from "react-image-gallery";

//import "react-image-gallery/styles/scss/image-gallery.scss";

import "react-image-gallery/styles/css/image-gallery.css";


export type GalleryProps = {
    dto: GalleryDTO;
}

export const Gallery: React.FC<GalleryProps> = (props) => {
    return (
        <div>
            <ImageGallery items={props.dto.items.map(image => ({ original: image.imageURL, description: image.caption, originalTitle: image.title }))} />
        </div>
    )
}