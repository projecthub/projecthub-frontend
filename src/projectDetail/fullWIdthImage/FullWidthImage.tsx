import React from 'react';
import { FullWidthImageDTO } from '../../types/Project';
// import { ClientInnerWidth, ScaleImage } from "react-scale-image"
import "./FullWidthImage.css"

export type FullWidthImageProps = {
    dto: FullWidthImageDTO;
}

export const FullWidthImage: React.FC<FullWidthImageProps> = (props) => {
    return (
        <div style={{ backgroundImage: 'url(' + props.dto.imageURL + ')', color: props.dto.color }} className='full-width-image'>
            <h1 className='ImageHeadline'>{props.dto.title}</h1>
            <h2>{props.dto.caption}</h2>
        </div>
    )
}