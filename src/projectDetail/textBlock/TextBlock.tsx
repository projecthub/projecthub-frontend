import React from 'react';
import { TextBlockDTO } from '../../types/Project';
import './TextBlock.css'

export type TextBlockProps = {
    dto: TextBlockDTO;
}

export const TextBlock: React.FC<TextBlockProps> = (props) => {
    const paragraphs = props.dto.content.split("\n")
    return (
        <div className='textblock-container'>
            <h1 className='TextHeadline'>{props.dto.title}</h1>
            {paragraphs.map(block => <p>{block}</p>)}
        </div>
    )
}

