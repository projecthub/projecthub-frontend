import { ArrowBack, ExpandMore } from '@material-ui/icons';
import React from 'react';
import { Project } from '../../types/Project';
import LocationOnIcon from '@material-ui/icons/LocationOn';
import AccessTimeIcon from '@material-ui/icons/AccessTime';
// import { ClientInnerWidth, ScaleImage } from "react-scale-image"
import "./ProjectDetailHeader.css"
import TagsComponent from '../../shared/TagsComponent/TagsComponent';
import { Accordion, AccordionDetails, AccordionSummary, Button } from '@material-ui/core';

export type ProjectDetailHeaderProps = {
    project: Project;
    onCloseDetailClicked: () => void;
    setRoute: (newRoute: string[]) => void;
}

export const ProjectDetailHeader: React.FC<ProjectDetailHeaderProps> = (props) => {
    return (
        <Accordion>

            <AccordionSummary
                expandIcon={<ExpandMore />}
                aria-controls="panel1a-content"
                id="panel1a-header"
            >
                <div className="project-detail-header-close-button" onClick={() => {
                    props.onCloseDetailClicked()
                }}>
                    <ArrowBack />
                </div>
                <h1 className='title-in-header'>{props.project.name}</h1>
            </AccordionSummary>
            <AccordionDetails>
                <div className="project-header-detail-container">
                    <span className="project-header-meta-span">
                        <span className="time-and-place-text"><AccessTimeIcon style={{ fontSize: 18, color: "#0987C6" }} className="icon"></AccessTimeIcon>{props.project.time}</span>
                        <span className="time-and-place-text"><LocationOnIcon style={{ fontSize: 18, color: "#0987C6" }} className="icon"></LocationOnIcon>{props.project.location}</span>

                    </span>
                    <TagsComponent tags={props.project.tags} setRoute={props.setRoute} />
                    <p>
                        <Button variant="contained" color="primary">
                            Contact us
                        </Button>
                    </p>
                </div>
            </AccordionDetails>
        </Accordion>
    )

}