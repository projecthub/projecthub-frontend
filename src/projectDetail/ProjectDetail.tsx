import React, { useState } from 'react';
import { isFullWidthImageDTO, isGalleryDTO, isTextBlockDTO, Project } from '../types/Project';
import { TextBlock } from './textBlock/TextBlock'
import { ProjectDetailHeader } from './projectDetailHeader/ProjectDetailHeader'
import { Logic } from '../logic';
import { FullWidthImage } from './fullWIdthImage/FullWidthImage';
import "./projectDetail.css"
import { Gallery } from './gallery/Gallery';

export type ProjectDetailProps = {
    projectID: string;
    logic: Logic;
    onCloseClicked: () => void;
    onContactClicked: () => void;
    setRoute: (newRoute: string[]) => void;
}
export const ProjectDetail: React.FC<ProjectDetailProps> = (props) => {
    const [project, setProject] = useState<Project | null>(null)
    if (!project) {
        props.logic.getProjectDetail(props.projectID).then((loadedProject) => {
            setProject(loadedProject)
        })
        // render loading page if not yet loaded
        return <p>loading...</p>
    } else {
        return (
            <div className="project-detail-main">
                <ProjectDetailHeader project={project} onCloseDetailClicked={props.onCloseClicked} setRoute={props.setRoute} />
                {project.content.map((component) => {
                    if (isTextBlockDTO(component)) {
                        return <TextBlock dto={component} />
                    } else if (isFullWidthImageDTO(component)) {
                        return <FullWidthImage dto={component} />
                    } else if (isGalleryDTO(component)) {
                        return <Gallery dto={component} />
                    } else {
                        return <p>unknown type</p>
                    }
                })}
            </div>
        )
    }
}