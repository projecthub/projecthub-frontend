import { ProjectPreviewData } from "../projectPreview/ProjectPreview";
import "./ProjectList.css";
import constants from "../logic/constants"
import ProjectPreviewCard from '../projectPreview/projectPreviewCard/ProjectPreviewCard';
import { ProjectDetail } from '../projectDetail/ProjectDetail';
import { Logic } from '../logic';

type ProjectListProps = {
    projects: ProjectPreviewData[];
    route: string[];
    setRoute: (newPath: string[]) => void;
    logic: Logic;
}

const focusedProjectIDIdentifier = "focusedProjectID=";
export const ProjectList: React.FC<ProjectListProps> = (props) => {
    // render a list of projects. Use two columns on desktop and one column on mobile.
    const lastRouteEl = props.route[props.route.length - 1]
    if (lastRouteEl?.startsWith(focusedProjectIDIdentifier)) {
        const focusedProjectID = lastRouteEl.substring(focusedProjectIDIdentifier.length)
        //render the focused project using its id
        if (constants.layout === "desktop") {
            return (
                <div className="project-detail-container-desktop">
                    <ProjectDetail logic={props.logic} projectID={focusedProjectID}
                        onCloseClicked={() => props.setRoute(props.route.slice(0, props.route.length - 1))}
                        onContactClicked={() => { }} setRoute={props.setRoute} />
                </div>
            )
        } else {
            return (
                <div className="project-detail-container-mobile">
                    <ProjectDetail logic={props.logic} projectID={focusedProjectID}
                        onCloseClicked={() => props.setRoute(props.route.slice(0, props.route.length - 1))}
                        onContactClicked={() => { }} setRoute={props.setRoute} />
                </div>
            )
        }
    } else {
        // else render a list of project previews
        const columns: React.ReactNode[] = []
        for (let columnID = 0; columnID < constants.columnCount; columnID++) {
            columns.push((
                <div className="list-column">
                    {props.projects.filter((_, index) => index % constants.columnCount === columnID).map(projectData => {
                        return (
                            <ProjectPreviewCard data={projectData} onFocus={() => {
                                props.setRoute([...props.route, focusedProjectIDIdentifier + projectData.projectID])
                            }}
                                width="var(--card-width)" setRoute={props.setRoute} />
                        )
                    })}
                </div>
            ))
        }
        return (
            <div className="list-column-container">
                {columns}
            </div>
        )
    }
}