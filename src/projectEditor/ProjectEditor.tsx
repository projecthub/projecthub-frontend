import { FC, useState } from "react";
import { Logic } from "../logic";
import { isFullWidthImageDTO, isGalleryDTO, isTextBlockDTO, Project, ProjectDetailComponentDTO } from "../types/Project";
import { BackgroundImageTextBlockEditor } from "./backgroundImageTextblockEditor/BackgroundImageTextBlockEditor";
import { GalleryEditor } from "./galleryEditor/GalleryEditor";
import { TextBlockEditor } from "./textBlockEditor/TextBlockEditor";

type ProjectEditorProps = {
    projectID: string;
    logic: Logic;
}
export const ProjectEditor: FC<ProjectEditorProps> = props => {
    const [project, setProject] = useState<Project | null>(null);
    if(!project){
        props.logic.getProjectDetail(props.projectID).then(projectDetail=>{
            setProject(projectDetail)
        })
    }
    if (project) {
        const setNthComponent = (n: number, newComponent: ProjectDetailComponentDTO) => {
            project.content[n] = newComponent
            setProject(project)
        }
        const saveProject = () => {
            props.logic.updateProject(props.projectID, project)
        }
        return (
            <div>
                <h1>Edit project {project.name}</h1>
                {
                    project.content.map((component, n) => {
                        if (isTextBlockDTO(component)) {
                            return <TextBlockEditor textBlockData={component} setTextBlockData={(c) => setNthComponent(n, c)} />
                        } else if (isFullWidthImageDTO(component)) {
                            return <BackgroundImageTextBlockEditor fullWidthImageData={component} setFullWidthData={(c) => setNthComponent(n, c)} />
                        } else if (isGalleryDTO(component)) {
                            return <GalleryEditor galleryData={component} setGalleryData={(c) => setNthComponent(n, c)} />
                        }

                    })
                }
            </div>
        )
    } else {
        props.logic.getProjectDetail(props.projectID).then(loadedData => {
            setProject(loadedData)
        })
        return <p>loading details...</p>
    }
}