import React from "react";
import { GalleryDTO } from "../../types/Project";
type GalleryEditorProps = {
    galleryData: GalleryDTO
    setGalleryData: (newData: GalleryDTO) => void
}

export const GalleryEditor: React.FC<GalleryEditorProps> = props => {
    // render the current value and call the update function on change
    return (
        <div>
            edit gallery
        </div>
    )
}