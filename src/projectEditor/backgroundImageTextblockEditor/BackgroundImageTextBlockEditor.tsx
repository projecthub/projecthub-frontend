import React from "react";
import { FullWidthImageDTO } from "../../types/Project";
type BackgroundImageTextBlockEditorProps = {
    fullWidthImageData: FullWidthImageDTO
    setFullWidthData: (newData: FullWidthImageDTO) => void
}

export const BackgroundImageTextBlockEditor: React.FC<BackgroundImageTextBlockEditorProps> = props => {
    // render the current value and call the update function on change
    return (
        <div>
            edit text block
        </div>
    )
}