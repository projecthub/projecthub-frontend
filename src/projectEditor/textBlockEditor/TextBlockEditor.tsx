import React from "react";
import { TextBlockDTO } from "../../types/Project";
type TextBlockEditorProps = {
    textBlockData: TextBlockDTO
    setTextBlockData: (newData: TextBlockDTO) => void
}

export const TextBlockEditor: React.FC<TextBlockEditorProps> = props => {
    // render the current value and call the update function on change
    return (
        <div>
            edit text block
        </div>
    )
}