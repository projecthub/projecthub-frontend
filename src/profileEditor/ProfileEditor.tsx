import { Button } from '@material-ui/core';
import React from 'react';
import { Logic } from '../logic';
import TagSelector from '../shared/tagSelector/TagSelector';
import { TextFieldEditor } from '../shared/textFieldEditor/TextFieldEditor';
import { Profile } from '../types/Profile';
import "./profile-editor.css"
type ProfileEditorProps = {
    logic: Logic
    onFinish: () => void
};

export const ProfileEditor: React.FC<ProfileEditorProps> = props => {
    const { logic } = props;
    const [profile, setProfile] = React.useState<Profile | null>(null);
    if (!profile) {
        logic.getCurrentUserID().then(userID => {
            logic.getProfile(userID).then(profile => {
                setProfile(profile);
            });
        });
        return <div> loading...</div>
    }
    return (
        <div className="profile-editor-main">
            <h2>Edit profile</h2>
            <TextFieldEditor
                value={profile.name}
                label="Name"
                updateValue={name => {
                    setProfile({ ...profile, name: name });
                }
                }
            />
            <TextFieldEditor
                value={profile.eMail}
                label="E-Mail"
                updateValue={eMail => {
                    setProfile({ ...profile, eMail: eMail });
                }
                }
            />

            <TextFieldEditor
                value={profile.description}
                label="description"
                multiline
                updateValue={description => {
                    setProfile({ ...profile, description: description });
                }
                }
            />
            <div className="input-container-card">
                <h3>Interests</h3>
                <TagSelector onUpdate={tags => {
                    profile.interests = tags;
                    setProfile(profile);
                }} />
            </div>
            <Button variant="contained" color="primary" onClick={(e) => props.onFinish()}>
                Finish
            </Button>

        </div>
    );
};