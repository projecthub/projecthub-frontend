import { FC } from 'react';
import './TagsComponent.css';

type tagComponentData = {
    tags: string[],
    selected?: string[],
    setRoute: (newRoute: string[]) => void
}

const tagComponent = (tag: string, isSelected: boolean, setRoute: (newRoute: string[]) => void) => {
    const onTagClick = () => {
        setRoute(["home", "query=" + tag])
    }
    return <span onClick={onTagClick} key={tag} className={`tag-text ${isSelected ? "selected" : ""}`}>#{tag}</span>
}


const TagsComponent: FC<tagComponentData> = (props) => {
    return (
        <div className="tag-list">
            {props.tags.map((tag, num, arr) => tagComponent(tag, props.selected?.includes(tag) || false, props.setRoute))}
        </div>
    );
}

export default TagsComponent;