import React from 'react';
import TextField from '@material-ui/core/TextField';
import "./textFieldEditor.css"

type TextFieldProps = {
    value: string;
    updateValue?: (value: string) => void;
    // returns error message if validation fails
    validate?: (value: string) => string | null;
    label?: string;
    multiline?: boolean
}

export const TextFieldEditor: React.FC<TextFieldProps> = (props) => {
    const { value, updateValue, validate } = props;
    const onValueChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        updateValue?.(event.target.value);
    }
    return (
        <div className="input-container-card">
            {props.children}
            <form>
                <TextField
                    error={validate ? validate(value) !== null : false}
                    value={value}
                    onChange={onValueChange}
                    fullWidth
                    multiline={props.multiline}
                    helperText={validate?.(value)}
                    label={props.label}
                />
            </form>
        </div>
    )
}