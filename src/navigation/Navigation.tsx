import { BottomNavigation, BottomNavigationAction } from '@material-ui/core'
import { AccountBox, Favorite, Home, Map, Settings } from '@material-ui/icons'
import React from 'react'
import { Home as HomeComponent } from '../home/Home'
import { Logic } from '../logic'
import { ProfileViewer } from '../profileViewer/ProfileViewer'
import { ProjectDetail } from '../projectDetail/ProjectDetail'
import "./navigation.css"
import { SideNavigator } from './sideNavigator/SideNavigator'


type NavigationProps = {
    layout: "mobile" | "desktop",
    route: string[],
    setRoute: (newRoute: string[]) => void,
    logic: Logic
}
type TabValue = "home" | "favorite" | "map" | "profile" | "settings"

const navigationItems = [
    {
        title: "Home",
        icon: <Home />,
        key: "home"
    },
    {
        title: "Favorites",
        icon: <Favorite />,
        key: "favorite"
    },
    {
        title: "Map",
        icon: <Map />,
        key: "map"
    },
    {
        title: "Profile",
        icon: <AccountBox />,
        key: "profile"
    },
    {
        title: "Settings",
        icon: <Settings />,
        key: "settings"
    }
]


export const Navigation: React.FC<NavigationProps> = props => {

    const selectedKey: TabValue = props.route[0] as any
    const selectedTab = (() => {
        switch (selectedKey) {
            case 'home':
                return <HomeComponent logic={props.logic} route={props.route} setRoute={props.setRoute} />
            case 'favorite':
                return <ProjectDetail logic={props.logic} projectID="1234" onCloseClicked={() => { }} onContactClicked={() => { }} setRoute={props.setRoute} />
            case 'settings':
                return <p>Settings</p>
            case 'profile':
                return <ProfileViewer logic={props.logic} route={props.route} setRoute={props.setRoute} />
            default:
                return null
        }
    })()
    const onTabChange = (newValue: TabValue) => {
        props.setRoute([newValue])
    }
    switch (props.layout) {
        case 'mobile':
            return (
                <div className="main-div-mobile">
                    <div className="mobile-content-container">
                        {selectedTab}
                    </div>
                    <BottomNavigation className="bottom-navigation"

                        value={selectedKey}
                        onChange={(ev, newVal) => onTabChange(newVal)}>
                        {navigationItems.map(item => (
                            <BottomNavigationAction value={item.key} icon={item.icon} />
                        ))}
                    </BottomNavigation>
                </div>
            )
        case 'desktop':
            return (
                <div className="main-div-desktop">
                    <SideNavigator selectedKey={selectedKey} onChange={(newTitle) => onTabChange(newTitle as any)}>
                        {navigationItems}

                    </SideNavigator>
                    <div className="desktop-content-container">
                        {selectedTab}
                    </div>
                </div>
            )
    }

}