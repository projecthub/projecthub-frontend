import React from "react"
import { DesktopTab } from "./desktopTab/DesktopTab"

type SideNavigatorProps = {
    children: {
        icon: React.ReactNode;
        title: string;
        key?: string;
    }[];
    onChange: (newTitle: string) => void;
    selectedKey: string
}
export const SideNavigator: React.FC<SideNavigatorProps> = props => {
    const setSelectedTabTitle = (newTitle: string) => {
        props.onChange(newTitle)
    }
    return (

        <div className="navigation-container">
            {
                props.children.map((tab) => <DesktopTab key={tab.key || tab.title} icon={tab.icon} title={tab.title} selected={props.selectedKey === (tab.key || tab.title)} onClick={() => setSelectedTabTitle(tab.key || tab.title)} />)
            }
        </div>
    )
}