import React from "react"
import "./desktopTab.css"

type DesktopTabProps = {
    title: string;
    icon: React.ReactNode;
    selected: boolean;
    onClick?: () => void;
}
export const DesktopTab: React.FC<DesktopTabProps> = props => {
    const mainClassName = "desktop-tab-main" + (props.selected ? " desktop-tab-main-selected" : "")
    return (
        <div className={mainClassName} onClick={(ev)=>props.onClick?.()}>
            <span className="icon-container">{props.icon}</span> <span className="title-container">{props.title}</span>
        </div >
    )
}
DesktopTab.defaultProps = {
    onClick: () => { }
}