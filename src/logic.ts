import { Router } from "./logic/routing";
import { Project } from "./types/Project";
import { ProjectPreviewData } from "./projectPreview/ProjectPreview";
import { Profile } from "./types/Profile";

const router = new Router()
export class Logic {
    router: Router = router
    async getProjectDetail(projectID: string): Promise<Project> {
        return {
            id: projectID,
            content: [
                {
                    color: 'white',
                    title: 'Find a project to join',
                    caption: 'At ProjectHub you can easily get in contact with your fellow students',
                    type: 'FullWidthImage',
                    imageURL: 'https://www.pxwall.com/wp-content/uploads/2018/06/Wallpaper%20milky%20way,%20stars,%205k,%20Space%20888192530.jpg'
                },
                {
                    title: "Why",
                    content: `
                    - Many students have great ideas
                    - Or want to join a side project but don't know how to find one
                    - Especially in times of Covid-19 it may be hard to get in contact with new people
                    
                    
                    `,
                    type: 'TextBlock'
                },
                {
                    type: "FullWidthImage",
                    title: "Join now!",
                    caption: "It's easy to insert your own project",
                    imageURL: "https://drive.google.com/uc?export=view&id=1VOql-zzxhucrogaz5w85M9DEZ-uNqcox",
                    color: "black"
                },
                {
                    type: "TextBlock",
                    title: "How it works",
                    content: `
                    - Create an account
                    - Add your interests and skills
                    - Browse for exciting new projects
                    `
                },
                {
                    type: "TextBlock",
                    title: "Have your own idea?",
                    content: `
                    - Create a new project
                    - Add pictures and description
                    - Say what people you are looking for
                    - Share your progress
                    `
                },
                {
                    type: "TextBlock",
                    title: "How we are organized",
                    content: `
                    - We meet circa every week to code
                    - We sometimes do sessions where we do further plans
                    `
                },
                {
                    type: "TextBlock",
                    title: "How TUM can profit",
                    content: `
                    - More students will be able to found groups and do cool stuff
                    - Student projects will be more present to the public
                    `
                },
                {
                    type: "TextBlock",
                    title: "Our roadmap",
                    content: `
                    - Get MVP ready
                    - Contact existing projects (Hyperloop, TUM Boring...) to get first content
                    - Get in touch wiht students!
                    `
                },
                {
                    type: "Gallery",
                    title: "Random Images",
                    items: [
                        {
                            imageURL: "https://upload.wikimedia.org/wikipedia/commons/1/15/Hyperloop_all_cutaway.png",
                            title: "Hyperloop",
                            caption: "Der Hyperloop ist eigentlich ganz schön toll."
                        },
                        {
                            imageURL:"https://upload.wikimedia.org/wikipedia/commons/6/6e/Elektronenmikroskop.jpg",
                            title: "Elektronenmikroskop",
                            caption: "Wir bauen ein Elektronenmikroskop!"
                        },
                    ]
                },
            ],
            location: "Munich",
            name: "ProjectHub",
            tags: ["organization", "networking", "TUM"],
            time: "24/7"
        }
    }
    async getSearchResults(searchProps: { query: string }, range: { start: number, end: number } = { start: 0, end: -1 }): Promise<ProjectPreviewData[]> {
        return [
            {
                description: "We want to simulate how a chain swings in julia!",
                imageURLs: ["https://julialang.org/assets/infra/logo.svg"],
                location: "Garching",
                name: "Swinging Chain",
                projectID: "1234",
                tags: ["physics", "simulation", "julialang"],
                time: "Mon 17:30"
            },
            {
                description: "We build a platform to connect TUM students with project ideas",
                imageURLs: ["https://upload.wikimedia.org/wikipedia/commons/0/08/P_cursive.gif"],
                location: "Munich",
                name: "ProjectHub",
                projectID: "3141",
                tags: ["organization", "networking", "TUM"],
                time: "Sat 8:30"
            },
            {
                description: "Wir wollen selbst ein Elektronenmikroskop basteln! Und das alles mit einer Anleitung aus YouTube.",
                imageURLs: ["https://upload.wikimedia.org/wikipedia/commons/6/6e/Elektronenmikroskop.jpg"],
                location: "Munich",
                name: "Elektronenmikroskop",
                projectID: "3141",
                tags: ["physics", "tech"],
                time: "Mon 17:30"
            },
            {
                description: "We build a flying-wing aircraft including its electronics from scratch",
                imageURLs: ["https://drive.google.com/uc?export=view&id=1-yTUe40GS0L8Rz8EBjnhxrClPE5hocjr", "https://drive.google.com/uc?export=view&id=1ny7M0GriFxGMEIsce4-ZnJXJnzeRhqQI"],
                location: "Munich",
                name: "Flying wing",
                projectID: "2718",
                tags: ["modellbau", "miroelectronics", "tech"],
                time: "Mi 19:30"
            }
        ]
    }

    /**
     * creates a new project for the current user
     * @returns newly created project id
     */
    async createProject() {
        return "1234"
    }
    /**
     * 
     * @param id - id of the project to update
     * @param updatedProjectData - the new project data
     * @returns whether the update was successful
     */
    async updateProject(id: string, updatedProjectData: Project) {
        return true
    }
    async getProfile(profileID: string): Promise<Profile> {
        return {
            eMail: "peter.schlonz@example.com",
            id: profileID,
            interests: ["tech", "it"],
            name: "Peter Schlonz",
            profileImageURL: "",
            username: "schlonzpeter01",
            description: "A very intelligent person",
            canEdit: true
        }
    }
    async getProfileID(username: string) {
        return "1234"
    }
    async getCurrentUserID() {
        return "1234"
    }
}