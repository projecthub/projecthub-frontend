export type Profile={
    id:string
    username:string
    name:string
    eMail:string
    interests:string[]
    profileImageURL:string
    description:string
    canEdit:boolean
}