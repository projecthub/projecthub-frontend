export type Project = {
    id: string,
    name: string,
    /** list of description objects */
    content: ProjectDetailComponentDTO[],
    time: string,
    location: string,
    tags: string[],
}


type ProjectDetailComponentDTOBase = {
    type: "FullWidthImage" | "Gallery" | "TextBlock";
}
export type FullWidthImageDTO = ProjectDetailComponentDTOBase & {
    imageURL: string;
    color: string;
    title?: string;
    caption?: string;
}
export const isFullWidthImageDTO = (dto: ProjectDetailComponentDTOBase): dto is FullWidthImageDTO => {
    return dto.type === "FullWidthImage";
}
export type TextBlockDTO = ProjectDetailComponentDTOBase & {
    title: string;
    content: string;
}
export const isTextBlockDTO = (dto: ProjectDetailComponentDTOBase): dto is TextBlockDTO => {
    return dto.type === "TextBlock";
}
export type GalleryDTO = ProjectDetailComponentDTOBase & {
    items: {
        title: string;
        caption: string;
        imageURL: string;
    }[]
}
export const isGalleryDTO = (dto: ProjectDetailComponentDTOBase): dto is GalleryDTO => {
    return dto.type === "Gallery";
}

export type ProjectDetailComponentDTO = FullWidthImageDTO | TextBlockDTO | GalleryDTO;