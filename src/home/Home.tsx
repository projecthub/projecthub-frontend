import React, { useRef, useState } from 'react';
import { Logic } from '../logic';
import { ProjectList } from '../projectList/ProjectList';
import { ProjectPreviewData } from '../projectPreview/ProjectPreview';
import SearchIcon from '@material-ui/icons/Search';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import './home.css';

export type HomeState = {
    query: string,
    focusedProjectID?: string
}
export type HomeProps = {
    logic: Logic;
    route: string[]
    setRoute: (newRoute: string[]) => void
}

export const Home: React.FC<HomeProps> = (props) => {
    const [resultList, setResultList] = useState<ProjectPreviewData[]>([])
    const inputRef = useRef<HTMLInputElement>(null)
    if (props.route.length <= 1) {
        props.setRoute(["home", "query="])
        return <p>wait a moment...</p>
    }
    const query = props.route[1].replace("query=", "")
    if (resultList.length === 0)
        props.logic.getSearchResults({ query: query }).then((results) => {
            setResultList(results)
        })

    const onSearchInput: React.KeyboardEventHandler = (event) => {
        if (event.key === "Enter") {
            props.setRoute(["home", "query=" + inputRef.current?.value])
        }
    }
    if (inputRef.current) {
        // set value on rerender
        inputRef.current.value = query
    }
    // render the home screen. Render a search bar and call setResultList to show search results
    return (
        <div >
            <div className="searchFieldContainer">
                <span className="searchField" >
                    <SearchIcon className="searchIcon"></SearchIcon>
                    <input ref={inputRef} onKeyDown={onSearchInput} placeholder="Search" ></input>
                    <button onClick={(e) => { props.setRoute(["home", "query=" + inputRef.current?.value]) }}>
                        <ArrowForwardIcon className="arrowIcon"></ArrowForwardIcon>
                    </button>
                </span>
            </div>

            <ProjectList logic={props.logic} projects={resultList} route={props.route} setRoute={props.setRoute} />
        </div>
    )
}