import React, { useState } from "react";
import { Logic } from "../logic";
import { ProfileEditor } from "../profileEditor/ProfileEditor";
import { ProjectList } from "../projectList/ProjectList";
import { ProjectPreviewData } from "../projectPreview/ProjectPreview";
import { Profile } from "../types/Profile";
import { ProfileData } from "./profileData/ProfileData";
import "./profileViewer.css";

export type ProfileViewerProps = {
    logic: Logic
    profileID?: string
    route: string[]
    setRoute: (newRoute: string[]) => void
}
export const ProfileViewer: React.FC<ProfileViewerProps> = props => {
    const [profile, setProfile] = useState<Profile | null>(null)
    const [myProjects, setMyProjects] = useState<ProjectPreviewData[] | null>(null);
    if (props.route.length > 2 && props.route[props.route.length - 2] === "editProfile") {
        return <ProfileEditor logic={props.logic} onFinish={()=>{
            props.setRoute(props.route.slice(0,props.route.length-2))
        }}/>
    }
    if (profile) {
        return (
            <div className="profile-viewer-main">
                <ProfileData profile={profile} setRoute={props.setRoute} />
                {myProjects ? <ProjectList logic={props.logic} projects={myProjects} route={props.route} setRoute={props.setRoute} /> : <p>loading my projects...</p>}
            </div>
        )
    } else {
        if (props.profileID) {
            props.logic.getProfile(props.profileID).then(data => {
                props.logic.getSearchResults({ query: "@" + props.profileID }).then(data => {
                    setMyProjects(data)
                })
                setProfile(data)
            })
        } else {
            props.logic.getCurrentUserID().then((id) => {
                props.logic.getProfile(id).then((data) => {
                    setProfile(data)

                    props.logic.getSearchResults({ query: "@" + id }).then(data => {
                        setMyProjects(data)
                    })
                })
            })
        }
        return <p>wait a moment...</p>
    }
}