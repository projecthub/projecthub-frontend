import { Avatar, Button } from "@material-ui/core";
import React from "react";
import TagsComponent from "../../shared/TagsComponent/TagsComponent";
import { Profile } from "../../types/Profile";
import "./profileData.css";

export type ProfileDataProps = {
    profile: Profile,
    setRoute: (newRoute: string[]) => void,
}

const ProfileSection: React.FC<{ title: string }> = props => (
    <div className="profile-section">
        <h3 className="profile-section-title">{props.title}</h3>
        {props.children}
    </div>
)
export const ProfilePreview: React.FC<{ profile: Profile }> = props => (
    <div className="profile-preview-main">
        <Avatar className="profile-avatar" alt={props.profile.name} src={props.profile.profileImageURL} />
        <div className="name-username-container">
            <h1 className="profile-name">{props.profile.name}</h1>
            <p className="username">{"@" + props.profile.username}</p>
        </div>
    </div>
)
export const ProfileData: React.FC<ProfileDataProps> = props => {
    return (
        <div className="profile-data-main">
            <ProfilePreview profile={props.profile} />
            <ProfileSection title="Description">
                {props.profile.description}
            </ProfileSection>
            <ProfileSection title="Interests">
                <TagsComponent tags={props.profile.interests} selected={[]} setRoute={props.setRoute} />
            </ProfileSection>
            <ProfileSection title="E-Mail">
                {props.profile.eMail}
            </ProfileSection>
            <Button color="primary" variant="contained" onClick={(e) => props.setRoute(["profile", "editProfile", props.profile.id])} >
                Edit profile
            </Button>
        </div>
    )
}
