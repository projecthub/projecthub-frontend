import React, { FC } from 'react';
import { ProjectPreviewProps } from '../ProjectPreview';
import TagsComponent from '../../shared/TagsComponent/TagsComponent';
import "./ProjectPreviewCard.css"
import AccessTimeIcon from '@material-ui/icons/AccessTime';
import LocationOnIcon from '@material-ui/icons/LocationOn';
import PreviewGalery from './PreviewGalery/PreviewGalery';


const ProjectPreviewCard: FC<ProjectPreviewProps> = (props) => {
    return (
        <div className="card" style={{ width: props.width }}>
            <div className="container">
                <h1>{props.data.name}</h1>
                <div className="containerIcon">
                    <AccessTimeIcon className="vertical-center icon-style"></AccessTimeIcon> <span className="iconText vertical-center">{props.data.time}</span>
                    <span className="tab"></span>
                    <LocationOnIcon className="vertical-center icon-style"></LocationOnIcon> <span className="iconText vertical-center">{props.data.location}</span>
                </div>
                <p>{props.data.description}</p>
                <PreviewGalery
                    imgURLs={props.data.imageURLs}
                    galeryID=""
                />
                <TagsComponent
                    tags={props.data.tags}
                    selected={["fun"]}
                    setRoute={props.setRoute}
                />

            </div>
            <button type="button" onClick={(event) => {
                props.onFocus();
            }}>Read more</button>
        </div>
    );
}

export default ProjectPreviewCard;