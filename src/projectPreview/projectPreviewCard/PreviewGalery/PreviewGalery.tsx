import { FC, useState } from 'react';
import "./PreviewGalery.css";
import ReactBnbGallery from 'react-bnb-gallery';
import 'react-bnb-gallery/dist/style.css'


type PreviewGaleryProps = {
    imgURLs: string[];
    galeryID: string
}

const PreviewGalery: FC<PreviewGaleryProps> = (props) => {
    var displayed: number = Math.min(4, props.imgURLs.length);

    const [openFull, setOpenFull] = useState({ displayed: false, index: 0 });

    const closeSlide = () => {
        setOpenFull({ displayed: false, index: 0 });
    }

    const openSlide = (index: number) => {
        setOpenFull({ displayed: true, index: index });
    }

    if (openFull.displayed) {
        return (
            <ReactBnbGallery
                show={openFull.displayed}
                photos={props.imgURLs}
                onClose={closeSlide}
                activePhotoIndex={openFull.index}
            />
        );
    }

    return props.imgURLs.length === 0 ? (<></>) : (
        <div className="preview-galery">
            <div className="preview-galery-img-container">
                {props.imgURLs.slice(0, displayed).map(
                    (imgURL, num) => {
                        return num < 3 ? (
                            <div key={num} className="preview-galery-img">
                                <img alt="galleryimage" className={"preview-galery-img"} src={imgURL} onClick={() => { openSlide(num) }} />
                            </div>
                        ) : (
                            <span key={num} className="overlay" onClick={() => { openSlide(num) }}>
                                <span className="overlay-text">{"+" + (props.imgURLs.length - 3)}</span>
                            </span>
                        );
                    }
                )}
            </div>
        </div>
    );
}

export default PreviewGalery;