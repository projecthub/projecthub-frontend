import React from 'react';
import "./ProjectPreview.css"
import ProjectPreviewCard from './projectPreviewCard/ProjectPreviewCard';

export type ProjectPreviewData = {
    projectID: string;
    name: string;
    time: string;
    location: string;
    description: string;
    tags: string[];
    imageURLs: string[];
}

export type ProjectPreviewProps = {
    data: ProjectPreviewData
    /** function to be called when the "read more"-button is clicked*/
    onFocus: () => void;
    setRoute: (newRoute: string[]) => void;
    width?: string | number;
}

export const ProjectPreview: React.FC<ProjectPreviewProps> = (props) => {
    // render a project preview card
    return (
        // Not needed anymore, implemented from ProjectPreviewCard.tsx
        // <div className="preview-box">
        //     {props.data.name}
        // </div>
        <div key={props.data.projectID}>
            <ProjectPreviewCard
                {...props}
            ></ProjectPreviewCard>
        </div>
    )
}
ProjectPreview.defaultProps = {
    width: "400px"
}