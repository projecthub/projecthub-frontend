const TARGET_CARD_WIDTH = 400

let cardWidth = 0
let contentWidth = 0
let navigationWidth = 160;
let gridGap = 15;
let columnCount = 0;
let searchWidth = 400;
const updateConstants = () => {
    const availableContentWidth = window.innerWidth - navigationWidth - 2 * gridGap
    switch (constants.layout) {
        case "desktop":
            if (availableContentWidth > 450) {
                columnCount = Math.min(Math.floor((availableContentWidth + gridGap) / (TARGET_CARD_WIDTH + gridGap)), 2)
                contentWidth = columnCount * (TARGET_CARD_WIDTH + gridGap) + gridGap
                cardWidth = TARGET_CARD_WIDTH
                searchWidth = 280;
            } else {
                columnCount = 1
                cardWidth = availableContentWidth
                contentWidth = availableContentWidth
                searchWidth = availableContentWidth - 200;
            }
            break;
        case "mobile":
            columnCount = 1
            contentWidth = document.documentElement.clientWidth
            cardWidth = document.documentElement.clientWidth - 10
            searchWidth = availableContentWidth - 50;
    }
    const root = document.documentElement
    root.style.setProperty("--card-width", cardWidth + "px")
    root.style.setProperty("--content-width", contentWidth + "px")
    root.style.setProperty("--navigation-width", navigationWidth + "px")
    root.style.setProperty("--grid-gap", gridGap + "px")
    root.style.setProperty("--column-count", columnCount.toString())
    root.style.setProperty("--default-color", "#0987C6")
    root.style.setProperty("--search-width", searchWidth + "px")
}
const constants = {
    get cardWidth() { return cardWidth },
    get contentWidth() { return contentWidth },
    get navigationWidth() { return navigationWidth },
    get layout() {
        if (/Android|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
            // true for mobile device
            return "mobile"
        } else {
            // false for not mobile device
            return "desktop"
        }
    },
    get gridGap() { return gridGap },
    get columnCount() { return columnCount; }
}

updateConstants()
window.addEventListener("resize", (ev) => {
    updateConstants()
})
export default constants