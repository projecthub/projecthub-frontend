
export class Router {
    onRouteChange: (newRoute: string[]) => void = (r) => { }
    get route() {
        try {
            const newRoute: string[] = JSON.parse(decodeURIComponent(window.location.hash.slice(1)))
            return newRoute
        }
        catch {
            console.log("invalid route, going to home")
            this.setRoute(["home"])
            return ["home"]
        }
    }
    constructor() {
        window.addEventListener("popstate", () => {
            this.onRouteChange(this.route)
        })

    }
    setRoute(newRoute: string[], title = "") {
        const serialized = encodeURIComponent(JSON.stringify(newRoute))
        window.location.hash = "#" + serialized
        //window.history.pushState(null, title, "#" + serialized)
    }
}